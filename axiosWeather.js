import axios from 'axios'

// Kutsutaan Axiosta 2 kertaa funktion sisällä
const getWeatherFor = async (city) => {
    const baseURL = "https://www.metaweather.com/api/location"
    try {
        const response = await axios.get(`${baseURL}/search/?query=${city}`)
        const woeid = response.data[0].woeid
        console.log(`Where on earth id (WOEID) for ${city} is ${woeid}`)
        const weather = await axios.get(`${baseURL}/${woeid}`)
        return weather.data
    } catch(err) {
        console.error('Oh noes!', err.message)
    }
}

const res = await getWeatherFor('helsinki')
console.log(res)

// Mapataan lista kaupunkeja pyynnöiksi, ja odotetaan pyyntöjen resolvautumista.
const cities = ['helsinki', 'london']

const responses = cities.map(async (city) => {
    const baseURL = "https://www.metaweather.com/api/location"
    const response = axios.get(`${baseURL}/search/?query=${city}`)
    return response
})

const data = await Promise.all(responses);
data.forEach((data) => {
    console.log(data.data)
})