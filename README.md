Axios (& Fetch) examples
-

How to use

1. Clone this repository with
    ```
    git clone git@gitlab.com:sokkanen/axios_examples.git

    OR

    git clone https://gitlab.com/sokkanen/axios_examples.git

2. Run npm install with

    ```
    npm install

    OR

    npm i
    ```