import axios from 'axios'

const url = "https://jsonplaceholder.typicode.com/todos/1"

try {
    const response = await axios.get(url)
    console.log(response.data)
} catch (error) {
    console.error(error.message)
}